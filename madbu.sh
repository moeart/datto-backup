#!/bin/bash
DBUDIR=$(realpath $(dirname $0))
SNAPID="19"
SNAPDEV="/dev/datto${SNAPID}"
SNAPTMP="/root/.xretia-dbu.tmp"
GZIPLV="6"

#--------------------
#     COMPITABLE
#--------------------
export NCURSES_NO_UTF8_ACS=1
export TERM=xterm


#--------------------
#   INTERNATIONAL
#--------------------
LANGS=(
    zh_CN "Chinese (Simplified)"
    zh_HK "Chinese (Tranditional)"
    en_US "English"
)
LANGU=$(LANGUAGE="en_US" dialog --backtitle "(c)2021 xRetia Labs" --default-item "$LANGUAGE" \
    --menu "Please Select Language" 0 0 0 "${LANGS[@]}" 2>&1 >/dev/tty)
if [ "$LANGU" = "" ]; then
    clear
    echo "**MADBU**: Byebye!"
    exit 1
fi
source "$DBUDIR/i18n/$LANGU.sh"



#--------------------
#      WELCOME
#--------------------
echo -en "\033]0;$TITLE\a"
dialog --backtitle "$TITLE" --title "$TITLE"  \
        --msgbox "$i18n_welcome" 15 60


#--------------------
#  CHECK SUPERUSER
#--------------------
if [ "$(id -u)" != "0" ]
then
    dialog --backtitle "$TITLE" --title "$i18n_error_title" \
        --msgbox "$i18n_need_root" 5 40
    exit 1
fi


#--------------------
#     CHECK DEPS
#--------------------
if [ "$(lsmod | grep -c datto)" = "0" ]
then
    dialog --backtitle "$TITLE" --title "$i18n_error_title" \
        --msgbox "$i18n_dattobd_missing" 9 60
    exit 1
fi
if [ "$(which pv)" = "" ] || [ "$(which pigz)" = "" ]
then
    dialog --backtitle "$TITLE" --title "$i18n_error_title" \
        --msgbox "$i18n_deps_missing" 9 60
    exit 1
fi


#--------------------
#   DISK SELECTION
#--------------------
MENU=""
NUMBER_LINE="$(lsblk -l | grep ' part ' | wc -l)"
for (( i = 1; i <= $NUMBER_LINE; i++ ))
do
    LSBLK="$(lsblk -l | grep ' part ' | sed "${i}q;d")"
    DISK="$(echo $LSBLK | awk '{ print $1 }')"
    MOUNT="$(echo $LSBLK | awk '{ print $7 }')"
    if [ "$MOUNT" = "" ]; then
        MENU+="$DISK $i18n_unmount "
    else
        MENU+="$DISK $MOUNT "
    fi
done
SDISK=$(dialog --backtitle "$TITLE" --menu "$i18n_select_disk" 5 60 0 $MENU 2>&1 >/dev/tty)
MOUNT=$(lsblk -n /dev/$SDISK -o MOUNTPOINT)
if [ "$SDISK" = "" ]
then
    dialog --backtitle "$TITLE" --title "$i18n_user_cancel" \
        --msgbox "$i18n_user_cancel2" 6 40
    exit 1
fi


#--------------------
#   COMPRESS LEVEL
#--------------------
MENU=""
NUMBER_LINE="$(lsblk -l | grep ' part ' | wc -l)"
for (( i = 0; i <= 9; i++ ))
do
    if [ "$i" = "6" ]; then
        MENU+="$i $i18n_complevel[6]($i18n_recommend) "
    else
        MENU+="$i $i18n_complevel[$i] "
    fi
done
GZIPLV=$(dialog --backtitle "$TITLE" --default-item '6' --menu "$i18n_comp_title" 5 60 0 $MENU 2>&1 >/dev/tty)
if [ "$GZIPLV" = "" ]
then
    dialog --backtitle "$TITLE" --title "$i18n_user_cancel" \
        --msgbox "$i18n_user_cancel2" 6 40
    exit 1
fi


#--------------------
#   SAVE LOCATION
#--------------------
DIMG=$(dialog --backtitle "$TITLE" --title "$i18n_save_title" --fselect `pwd` 6 70 0  2>&1 >/dev/tty)
if [ -d $DIMG ] || [ -f $DIMG ]
then
    dialog --backtitle "$TITLE" --title "$i18n_error_title" \
        --msgbox "$(printf "$i18n_save_lock" "$DIMG")" 10 60
    exit 1
fi


#--------------------
#   USER CONFIRM
#--------------------
dialog --backtitle "$TITLE" --title "$i18n_confirm_title" \
    --defaultno --yesno "$i18n_confirm_msg" 12 70  2>&1 >/dev/tty
if [ $? -eq 1 ]
then
    dialog --backtitle "$TITLE" --title "$i18n_user_cancel" \
        --msgbox "$i18n_user_cancel2" 6 40
    exit 1
fi


#--------------------
#   CHECK SNAPDEV
#--------------------
if [ "$MOUNT" != "" ]
then
    ls $SNAPDEV >/dev/null 2>&1
    if [ $? -eq 0 ]
    then
        dialog --backtitle "$TITLE" --title "$i18n_snapdevlock_title" \
            --defaultno --yesno "$(printf "$i18n_snapdevlock_msg" "$SNAPDEV")" 12 70
        if [ $? -eq 1 ]
        then
            dialog --backtitle "$TITLE" --title "$i18n_user_cancel" \
                --msgbox "$i18n_user_cancel2" 6 40
            exit 1
        fi
        #__destory_snapdev__
        dbdctl transition-to-incremental $SNAPID
        dbdctl destroy $SNAPID
    fi
fi


#--------------------
#     PREPARING
#--------------------
dialog --backtitle "$TITLE" --infobox "$i18n_preparing" 3 50
rm -f $DBUDIR/xretia-dbu.log >/dev/null 2>&1
touch $DBUDIR/xretia-dbu.log >/dev/null 2>&1
mkdir -p $(dirname $DIMG) >/dev/null 2>&1
sleep 1


#--------------------
#   SETUP SNAPDEV
#--------------------
if [ "$MOUNT" != "" ]
then
    dialog --backtitle "$TITLE" --infobox "$i18n_creating_snap" 3 50
    if [ "$MOUNT" != "" ] && [ -w "$MOUNT" ]
    then
        SNAPTMP="${MOUNT}/.xretia-dbu.tmp"
    fi
    dbdctl setup-snapshot /dev/$SDISK $SNAPTMP $SNAPID >>$DBUDIR/xretia-dbu.log 2>&1
    sleep 1
    if [ $? -eq 1 ]
    then
        dialog --backtitle "$TITLE" --title "$i18n_error_title" \
            --msgbox "$i18n_snapping_failed $DBUDIR/xretia-dbu.log" 6 40
        exit 1
    fi
fi


#--------------------
# BACKUP IN PROGRESS
#--------------------
DDSRC="$SNAPDEV"
if [ "$MOUNT" = "" ]; then
    DDSRC="/dev/$SDISK"
fi
(pv -n $DDSRC | dd bs=1M | pigz -$GZIPLV > $DIMG) 2>&1 | dialog --backtitle "$TITLE" --gauge "$(printf "$i18n_backuping" "$SDISK" "$DIMG")" 10 70 0


#--------------------
#  UNMOUNT SNAPDEV
#--------------------
function finish()
{
    chown $SUDO_UID:$SUDO_GID $DBUDIR/xretia-dbu.log
    chown $SUDO_UID:$SUDO_GID $DIMG
    clear
}
if [ "$MOUNT" != "" ]
then
    dialog --backtitle "$TITLE" --infobox "$i18n_merging" 3 50
    sleep 1
    dbdctl transition-to-incremental $SNAPID >>$DBUDIR/xretia-dbu.log 2>&1 && \
    dbdctl destroy $SNAPID >>$DBUDIR/xretia-dbu.log 2>&1
fi
if [ $? -eq 0 ]
then
    dialog --backtitle "$TITLE" --title "$i18n_finished" \
        --msgbox "$i18n_finished_msg \n$DIMG" 7 60
    finish
    exit 0
else
    dialog --backtitle "$TITLE" --title "$i18n_error_title" \
        --msgbox "$(printf "$i18n_backup_failed" "$DIMG" "$DBUDIR/xretia-dbu.log")" 10 70
    finish
    exit 1
fi
