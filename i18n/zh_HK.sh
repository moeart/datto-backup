#!/bin/bash
TITLE="MoeArt's Disk Backup Utilty"

#--------------------
# WELCOME
#--------------------
i18n_welcome="歡迎使用 $TITLE\n\n
這一款基於卷影驅動datto的磁盤備份程序，它可以直接從您當前運行中系統的磁盤上生成鏡像，並且盡最大的可能保持文件的完整性！ \n\n
任何與磁盤相關的操作都必須要保持小心謹慎！請不要以為卷影複製就很安全，請必須在頭腦清醒的狀態下使用本工具，作者不對用戶磁盤數據丟失負責！ \n\n
版權所有: (c)2021 xRetia Labs\n
版權所有: (c)2021 MoeArt Inc. - www.acgdraw.com\n
程序版本: 1.1b20201231"


#--------------------
# CHECK DEPS
#--------------------
i18n_dattobd_missing="卷影驅動未加載，請檢查是否安裝卷影驅動？\n\n
安裝教程: https://github.com/datto/dattobd/blob/master/INSTALL.md"

i18n_deps_missing="依賴組件 pv, pigz 未安裝！\n\n
安裝教程: sudo apt-get install -y pv pigz"


#--------------------
# DISK SELECTION
#--------------------
i18n_unmount="[未掛載]"
i18n_select_disk="請選擇需要備份的磁盤"


#--------------------
# COMPRESS LEVEL
#--------------------
i18n_recommend="推薦"
i18n_complevel="壓縮級別"
i18n_comp_title="請選擇壓縮級別（級別越高容量越小）"


#--------------------
# SAVE LOCATION
#--------------------
i18n_save_title="磁盤鏡像保存位置"
i18n_save_lock="鏡像位置: %s\n當前保存位置已被佔用，請更改其他存儲位置，再見！"


#--------------------
# USER CONFIRM
#--------------------
i18n_confirm_title="操作涉及重要數據，請三思而後行"
i18n_confirm_msg="當前操作將創建磁盤快照，創建快照前後的數據將暫時分離。期間程序依然可以繼續運行，並操作磁盤寫入數據。\n\n
在磁盤鏡像生成完成後，程序對磁盤數據做出的修改將合併到物理磁盤。 \n\n
雖然一般而言，不會對數據產生影響，但強烈建議您停止對磁盤持續寫入的程序，如 MySQL。 "


#--------------------
# CHECK SNAPDEV
#--------------------
i18n_snapdevlock_title="快照設備被佔用"
i18n_snapdevlock_msg="用於生成捲影的設備 %s 被佔用！如果不是由您創建的，您可以點擊“是”來強制卸載快照設備。\n\n
在強制卸載之前強烈建議您確認快照設備是否在使用，比如您已經執行了一個磁盤備份任務。 \
如果有任務在運行，強制卸載快照設備將輕則磁盤鏡像備份失敗，重則數據丟失！ "



#--------------------
# COMMON
#--------------------
i18n_error_title="發生了錯誤"
i18n_need_root="需要超級用戶權限！"
i18n_user_cancel="用戶取消了操作"
i18n_user_cancel2="您取消了備份操作，再見！"
i18n_preparing="正在準備工作環境 ... "
i18n_creating_snap="正在創建磁盤快照 ... "
i18n_snapping_failed="快照創建失敗！詳見日誌"
i18n_backuping="正在備份磁盤 %s 到 %s ..."
i18n_merging="正在合併磁盤快照 ... "
i18n_finished="備份完成"
i18n_finished_msg="磁盤鏡像備份完成，鏡像位置:"
i18n_backup_failed="磁盤鏡像備份完成，但合併磁盤快照時發生了錯誤！\n\n
鏡像位置: %s
詳見日誌: %s"

export LANGUAGE="zh_TW"
