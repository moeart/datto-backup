#!/bin/bash
TITLE="MoeArt's Disk Backup Utilty"

#--------------------
#      WELCOME
#--------------------
i18n_welcome="欢迎使用 $TITLE\n\n
这一款基于卷影驱动datto的磁盘备份程序，它可以直接从您当前运行中系统的磁盘上生成镜像，并且尽最大的可能保持文件的完整性！\n\n
任何与磁盘相关的操作都必须要保持小心谨慎！请不要以为卷影复制就很安全，请必须在头脑清醒的状态下使用本工具，作者不对用户磁盘数据丢失负责！\n\n
版权所有: (c)2021 xRetia Labs\n
版权所有: (c)2021 MoeArt Inc. - www.acgdraw.com\n
程序版本: 1.1b20201231"


#--------------------
#     CHECK DEPS
#--------------------
i18n_dattobd_missing="卷影驱动未加载，请检查是否安装卷影驱动？\n\n
安装教程: https://github.com/datto/dattobd/blob/master/INSTALL.md"

i18n_deps_missing="依赖组件 pv, pigz 未安装！\n\n
安装教程: sudo apt-get install -y pv pigz"


#--------------------
#   DISK SELECTION
#--------------------
i18n_unmount="[未挂载]"
i18n_select_disk="请选择需要备份的磁盘"


#--------------------
#   COMPRESS LEVEL
#--------------------
i18n_recommend="推荐"
i18n_complevel="压缩级别"
i18n_comp_title="请选择压缩级别（级别越高容量越小）"


#--------------------
#   SAVE LOCATION
#--------------------
i18n_save_title="磁盘镜像保存位置"
i18n_save_lock="镜像位置: %s\n当前保存位置已被占用，请更改其他存储位置，再见！"


#--------------------
#   USER CONFIRM
#--------------------
i18n_confirm_title="操作涉及重要数据，请三思而后行"
i18n_confirm_msg="当前操作将创建磁盘快照，创建快照前后的数据将暂时分离。期间程序依然可以继续运行，并操作磁盘写入数据。\n\n
在磁盘镜像生成完成后，程序对磁盘数据做出的修改将合并到物理磁盘。\n\n
虽然一般而言，不会对数据产生影响，但强烈建议您停止对磁盘持续写入的程序，如 MySQL。"


#--------------------
#   CHECK SNAPDEV
#--------------------
i18n_snapdevlock_title="快照设备被占用"
i18n_snapdevlock_msg="用于生成卷影的设备 %s 被占用！如果不是由您创建的，您可以点击“是”来强制卸载快照设备。\n\n
在强制卸载之前强烈建议您确认快照设备是否在使用，比如您已经执行了一个磁盘备份任务。\
如果有任务在运行，强制卸载快照设备将轻则磁盘镜像备份失败，重则数据丢失！"



#--------------------
#      COMMON
#--------------------
i18n_error_title="发生了错误"
i18n_need_root="需要超级用户权限！"
i18n_user_cancel="用户取消了操作"
i18n_user_cancel2="您取消了备份操作，再见！"
i18n_preparing="正在准备工作环境 ... "
i18n_creating_snap="正在创建磁盘快照 ... "
i18n_snapping_failed="快照创建失败！详见日志"
i18n_backuping="正在备份磁盘 %s 到 %s ..."
i18n_merging="正在合并磁盘快照 ... "
i18n_finished="备份完成"
i18n_finished_msg="磁盘镜像备份完成，镜像位置:"
i18n_backup_failed="磁盘镜像备份完成，但合并磁盘快照时发生了错误！\n\n
镜像位置: %s
详见日志: %s"

export LANGUAGE="zh_CN"
